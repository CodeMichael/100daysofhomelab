---
title: 11/06 - lets get (re)started
created: 2022-11-06
tags: 
  - 100daysofhomelab
  - RPI4
author: Michael Babler
---

## NixOS

I'm frustrated.  NixOS was exceptionally exciting on day one when I tired it on the Raspberry Pi. I've been trying to get a new build running and it keeps failing. The really aggravating part is that it has to run a kernel build, which on a RPI4 on powersave mode (kept locking up, POE fan isn't running for some reason), is taking forever and a day. Every time I make a change it takes hours to find out if something has changed. Most recently I'm recently trying to switch from the pi4 specific kernel to the mainline kernel.

My goals today are mostly around getting organized, and trying to get rolling on #100DaysOfHomelab again. Today I'm going to get Gitlab squared away including this.

```text
/nix/store/6ahp7jcb5x8b76j6jfkhzm6wwnd8za8c-binutils-2.39/bin/ld: warning: -z relro ignored
/nix/store/6ahp7jcb5x8b76j6jfkhzm6wwnd8za8c-binutils-2.39/bin/ld: warning: vmlinux has a LOAD segment with RWX permissions
  BTFIDS  vmlinux
FAILED: load BTF from vmlinux: Invalid argument
make: *** [../Makefile:1183: vmlinux] Error 255
make: *** Deleting file 'vmlinux'
error: builder for '/nix/store/hq97nzb72vhgvv0l3jz9081zb0xp0cbw-linux-5.15.32-1.20220331.drv' failed with exit code 2
error: 1 dependencies of derivation '/nix/store/kkglqdqhijp5grrqx195q6wz830nly5s-nixos-system-vecna-22.11pre423979.3bacde6273b.drv' failed to build
```

## Yubikeys

I have some new yubikeys, and I needed to move my gpg keys onto my new ones.  Seriously its a pain in the ass to get information about this. The tools are about as user unfriendly as it is possible to be, and the documentation is not great.

```shell
# Keys are normally kept in the vault, files were exported temporarily so that I could import them into my new yubikeys
gpg --import master.key master.sub.key sub.key
# now they're in the keyring
gpg --list-keys
gpg --list-secret-keys
gpg --edit-key ${my_key_fingerprint:?}
# in the gpg shell
# gpg> trust
# select 5 for ultimate trust
# gpg> keytocard
# select 1, supply the key password
# gpg> keytocard
# select 3
```
