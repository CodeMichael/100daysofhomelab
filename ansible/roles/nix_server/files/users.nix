{ config, ... }:
{
  users.users = {
    codemichael = {
      isNormalUser = true;
      description = "Michael"
      shell = pkgs.zsh;
      extraGroups = [
        "wheel"
        "docker"
      ];
      openssh.authorizedKeys.keys = [
        "sk-ssh-ed25519@openssh.com AAAAGnNrLXNzaC1lZDI1NTE5QG9wZW5zc2guY29tAAAAIIYQ1JkAFMNalIEMr0vg1WY7rXLmCZT/6PeewCD/usCVAAAABHNzaDo= codemichael@shar"
        "sk-ssh-ed25519@openssh.com AAAAGnNrLXNzaC1lZDI1NTE5QG9wZW5zc2guY29tAAAAIMZ5fkAN7N8ntIF1mtYIMKYdaXbIaQTnZ2Nr6CXqlS58AAAAB3NzaDo1Q2k= codemichael@shar"
      ];
    };
    root = {
      shell = pkgs.zsh;
      openssh.authorizedKeys.keys = [
        "sk-ssh-ed25519@openssh.com AAAAGnNrLXNzaC1lZDI1NTE5QG9wZW5zc2guY29tAAAAIIYQ1JkAFMNalIEMr0vg1WY7rXLmCZT/6PeewCD/usCVAAAABHNzaDo= codemichael@shar"
        "sk-ssh-ed25519@openssh.com AAAAGnNrLXNzaC1lZDI1NTE5QG9wZW5zc2guY29tAAAAIMZ5fkAN7N8ntIF1mtYIMKYdaXbIaQTnZ2Nr6CXqlS58AAAAB3NzaDo1Q2k= codemichael@shar"
      ];
      hashedPassword = "!";
    };
  };
}
