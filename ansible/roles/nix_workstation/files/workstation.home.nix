{ config, pkgs, ... }: {
  fonts.fontconfig.enable = true;
  nixpkgs.config.allowUnfree = true;
  nix = {
    enable = true;
    package = pkgs.nix;
    settings = {
      experimental-features = "nix-command flakes";
    };
  };
  manual.manpages.enable = false;
  home = {
    username = "codemichael";
    homeDirectory = "/home/codemichael";
    stateVersion = "22.05";
    sessionVariables = {
      EDITOR = "vim";
      VISUAL = "vim";
      MOZ_ENABLE_WAYLAND = 1;
      # NIXOS_OZONE_WL = 1;
    };
    packages = with pkgs; [
      bat
      bash
      bind
      bitwarden-cli
      chrome-gnome-shell
      element-desktop
      exa
      firefox-wayland
      flatpak
      font-awesome
      gamemode
      glances
      glow
      glxinfo
      gnomeExtensions.appindicator
      gnomeExtensions.caffeine
      gnomeExtensions.gesture-improvements
      gnomeExtensions.gtile
      gnomeExtensions.taildrop-send
      gnomeExtensions.tailscale-status
      gnome.dconf-editor
      gnome.gnome-tweaks
      gnupg
      httm
      # httpie
      jetbrains-mono
      mailspring
      mangohud
      mosh
      mplayer
      nerdfonts
      newsflash
      nixpkgs-fmt
      nordic
      pciutils
      pinentry-gnome
      polychromatic
      powerline
      powerline-fonts
      profile-sync-daemon
      protonvpn-cli
      protonvpn-gui
      signal-desktop
      sublime4
      tor-browser-bundle-bin
      thunderbird
      unzip
      usbutils
      yubikey-manager
      yubikey-manager-qt
      yubikey-personalization
      yubikey-personalization-gui
      #yubioath-desktop
      yubioath-flutter
      via
      vscode
      # (
      #   let
      #     my-python-packages = python-packages: with python-packages; [
      #       openrazer-daemon
      #       pandas
      #       python39Packages.dbus-python
      #       python39Packages.powerline
      #       python39Packages.protonvpn-nm-lib
      #       python39Packages.pyuv
      #       requests
      #     ];
      #     python-with-my-packages = python39.withPackages my-python-packages;
      #   in
      #   python-with-my-packages
      # )
      (pkgs.vim_configurable.customize {
        name = "vim";
        vimrcConfig = {
          customRC = ''
            set number
            call plug#begin()
            Plug 'LnL7/vim-nix'
            call plug#end()
          '';
          plug.plugins = with pkgs.vimPlugins ; [ vim-nix ];
        };
      })
    ];
  };
  services = {
    gpg-agent = {
      pinentryFlavor = "curses";
      enableSshSupport = true;
      enableExtraSocket = true;
      extraConfig = ''
        pinentry-program ${pkgs.pinentry-curses}/bin/pinentry-curses
      '';
    };
  };
  systemd.user.sessionVariables.SSH_AUTH_SOCK = "/run/user/1000/keyring/ssh";
  programs = {

    alacritty = {
      enable = true;
    };
    btop = {
      enable = true;
    };
    git = {
      enable = true;
      userName = "codemichael";
      userEmail = "codemichael@codemichael.net";
      signing = {
        key = "BE733E40433A84BD";
        signByDefault = true;
      };
    };
    gpg = {
      enable = true;
      # publicKeys = [{ source = ./codemichael.gpg.pub.key; }];
    };
    kitty = {
      enable = true;
      font = {
        name = "JetBrains Mono";
      };
      settings = {
        font_size = "16.0";
        # hide_window_decorations = "yes";
        dynamic_background_opacity = "yes";
        background_opacity = "0.8";
        term = "xterm-256color";
        wayland_titlebar_color = "background";
        tab_title_template = "{index}: {title}";
      };
      extraConfig = ''
        # Nord Colorscheme for Kitty
        # Based on:
        # - https://gist.github.com/marcusramberg/64010234c95a93d953e8c79fdaf94192
        # - https://github.com/arcticicestudio/nord-hyper

        foreground            #D8DEE9
        background            #2E3440
        selection_foreground  #000000
        selection_background  #FFFACD
        url_color             #0087BD
        cursor                #81A1C1

        # black
        color0   #3B4252
        color8   #4C566A

        # red
        color1   #BF616A
        color9   #BF616A

        # green
        color2   #A3BE8C
        color10  #A3BE8C

        # yellow
        color3   #EBCB8B
        color11  #EBCB8B

        # blue
        color4  #81A1C1
        color12 #81A1C1

        # magenta
        color5   #B48EAD
        color13  #B48EAD

        # cyan
        color6   #88C0D0
        color14  #8FBCBB

        # white
        color7   #E5E9F0
        color15  #ECEFF4

      '';
    };
    starship = {
      enable = true;
      settings = {};
    };
    tmux = {
      enable = true;
      clock24 = true;
      terminal = "screen-256color";
      # source ${pkgs.powerline}/share/tmux/powerline.conf
      # source ${pkgs.python39Packages.powerline}/lib/python3.9/site-packages/powerline/bindings/tmux/powerline.conf
      sensibleOnTop = false;
      extraConfig = ''
        source ${pkgs.powerline}/share/tmux/powerline.conf
      '';
    };
    vscode = {
      enable = true;
      # package = pkgs.vscodium;    # You can skip this if you want to use the unfree version
      extensions = with pkgs.vscode-extensions; [
        eamodio.gitlens
        bbenoist.nix
        jnoortheen.nix-ide
        # redhat.ansible
        ms-azuretools.vscode-docker
        vscodevim.vim
        davidanson.vscode-markdownlint
        yzhang.markdown-all-in-one
        oderwat.indent-rainbow
        codezombiech.gitignore
        # hediet.vscode-drawio
        hashicorp.terraform
        arcticicestudio.nord-visual-studio-code
        # wayou.vscode-todo-highlight
        gruntfuggly.todo-tree
      ];
      userSettings = {
        "vim.handleKeys" = {
          "<C-d>" = true;
          "<C-c>" = false;
          "<C-v>" = false;
          "<C-x>" = false;
          "<C-a>" = false;
          "<C-f>" = false;
          "<C-n>" = false;
        };
        "files.associations" = {
          "*.nomad" = "terraform";
          "*.hcl" = "terraform";
          "*.nomad.j2" = "terraform";
          "*.hcl.j2" = "terraform";
          "*.yml" = "ansible";
        };
        "yaml.customTags" = [
          "!vault"
        ];
        "terminal.integrated.fontFamily" = "Hack Nerd Font";
      };
    };
    zsh = {
      enable = true;
      initExtra = ''
        # eval $(ssh-agent) > /dev/null
        export GPG_TTY="$(tty)"
        eval "$(starship init zsh)"
        DISABLE_AUTO_TITLE="true"
      '';
      shellAliases = {
        ls = "exa --long --git --header";
        ssh = "mosh";
      };
      sessionVariables = {
        EDITOR = "vim";
        VISUAL = "vim";
        MOZ_ENABLE_WAYLAND = 1;
        # NIXOS_OZONE_WL = 1;
      };
      prezto = {
        # https://github.com/nix-community/home-manager/blob/master/modules/programs/zsh/prezto.nix
        enable = true;
        prompt.theme = "powerlevel10k";
        pmodules = [
          "archive"
          "autosuggestions"
          "completion"
          # "directory"
          "editor"
          # "environment"
          "git"
          "history"
          "history-substring-search"
          "prompt"
          "python"
          # "spectrum"
          # "ssh"
          "syntax-highlighting"
          # "terminal"
          # "tmux"
          "utility"
        ];
        extraConfig = ''
          source ~/.p10k.zsh
        '';
      };
    };
  };
}
