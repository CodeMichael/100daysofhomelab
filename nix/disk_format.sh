#!/run/current-system/sw/bin/bash

##
# This script formats the root disk
# including a EFI volume, a ZFS root
# and sets up ZFS volumes.
##

if [[ "$EUID" -ne 0 ]]; then
  echo "This must be run as root"
fi

# needed vars
## _root_disk -- /dev/disk/by-id/*
source ./server.env
# Test root disk
echo "ROOT_DISK: ${ROOT_DISK:no_disk_specified}"
# verify the device is disk-id
if [[ "${ROOT_DISK}" =~ ^\/dev\/disk\/by-id\/.*$ ]]; then
  # verify the item is an actual block device
  if [[ -b ${ROOT_DISK} ]]; then
    # if root disk exists
    _root_disk=${ROOT_DISK}
  else
    echo "Does not appear to be"
    echo "a block device."
    exit 1
  fi
else
  echo "This does not seem to be a /dev/disk/by-id"
  echo "Please specify the disk-id"
  exit 1
fi
# Test data disk
## Data disk does not need to exist
echo "DATA_DISK: ${DATA_DISK:no_disk_specified}"
if [[ test "${DATA_DISK}" ]]; then
  # verify the device is disk-id
  if [[ "${DATA_DISK}" =~ ^\/dev\/disk\/by-id\/.*$ ]]; then
    # verify the item is an actual block device
    if [[ -b ${DATA_DISK} ]]; then
      # if data disk exists
      _data_disk=${DATA_DISK}
    else
      echo "Does not appear to be"
      echo "a block device."
      exit 1
    fi
  else
    echo "This does not seem to be a /dev/disk/by-id"
    echo "Please specify the disk-id"
    exit 1
  fi
fi
#/needed vars

# Functions
function Fail() {
  echo $1
  exit 1
}

# partition disk
## blank the disk
sgdisk --zap-all ${_root_disk}
## create GPT boot partition
sgdisk -n1:1M:+1024M -t1:EF00 -c1:"UEFI Boot" ${_root_disk}
## swap partition
#parted ${_root_disk} -- mkpart primary linux-swap 512MiB 16GiB
sgdisk -n2:0:+32G -t2:8200 -c2:"Swap" ${_root_disk}
## zfs rpool
sgdisk -n3:0:0 -t3:BF01 -c3:"rpool" ${_root_disk}
## print it out
sgdisk --print ${_root_disk}
sync
sleep 2

# Format disks
## format boot as fat32
mkfs.vfat -F32 ${_root_disk}-part1 || Fail "couldn't format boot partition"
sync
## make swap
mkswap ${_root_disk}-part2 || Fail "coulnd't format swap"
swapon ${_root_disk}-part2
sync

## create zpool
zpool create -f           \
  -R /mnt                 \
  -o ashift=12            \
  -o autotrim=on          \
  -O acltype=posixacl     \
  -O relatime=on          \
  -O xattr=sa             \
  -O dnodesize=legacy     \
  -O normalization=formD  \
  -O mountpoint=none      \
  -O compression=lz4      \
  -O canmount=off         \
  -O devices=off          \
  rpool ${_root_disk}-part3 || Fail "couldn't create zpool"
sync

## create root datasets
zfs create -o canmount=off -o mountpoint=/ rpool/ROOT
zfs create -o mountpoint=/ -o canmount=noauto rpool/ROOT/nixos

zfs mount rpool/ROOT/nixos

zfs create -o canmount=on rpool/ROOT/nix
zfs create -o canmount=on rpool/ROOT/etc
zfs create -o canmount=on rpool/ROOT/var
zfs create -o canmount=on rpool/ROOT/var/log
zfs create -o canmount=on rpool/ROOT/var/lib
zfs create -o canmount=on rpool/ROOT/var/lib/docker


## mount boot
mkdir /mnt/boot
mount ${_root_disk}-part1 /mnt/boot

if [[ test "$_data_disk" ]]; then
  # unlike the root disk, we don't want to recreate the
  # data disk everytime we run this.  Leave the data
  # alone, if it exists.
  zpool import -d ${_data_disk} datapool || {

    zpool create -f           \
      -R /mnt                 \
      -o ashift=12            \
      -o autotrim=on          \
      -O acltype=posixacl     \
      -O relatime=on          \
      -O xattr=sa             \
      -O dnodesize=legacy     \
      -O normalization=formD  \
      -O mountpoint=none      \
      -O compression=lz4      \
      -O canmount=off         \
      -O devices=off          \
      datapool ${_data_disk} || Fail "couldn't create zpool"
    sync

    ## create userdata sets
    zfs create -o canmount=on datapool/home
    zfs create -o canmount=on datapool/root
    zfs create -o canmount=on datapool/srv

  }
else
  zfs create -o canmount=on rpool/home
  zfs create -o canmount=on rpool/root
  zfs create -o canmount=on rpool/srv
fi

