{ config, pkgs, ... }:
{
  services = {
    openssh.enable = true;
    tailscale.enable = true;
    zfs = {
      trim.enable = true;
      autoScrub = {
        enable = true;
        pools = [ "rpool" ];
      };
    };
  };
  virtualisation.docker.enable = true;
}